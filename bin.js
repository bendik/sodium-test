const sodium = require('sodium-native')
const minimist = require('minimist')
const fs = require('fs')

const argv = minimist(process.argv.slice(2), {
  alias: {
    keygen: 'k',
    sign: 's',
    secret: 'S',
    public: 'P',
    verify: 'v',
    output: 'o'
  },
  '--': true
})

const public = sodium.sodium_malloc(sodium.crypto_sign_PUBLICKEYBYTES)
const secret = sodium.sodium_malloc(sodium.crypto_sign_SECRETKEYBYTES)
const public_path = Buffer.from(argv.public || 'public.key')
const secret_path = Buffer.from(argv.secret || 'secret.key')

if (argv.keygen) {
  sodium.crypto_sign_keypair(public, secret)
  fs.writeFileSync(public_path, public.toString('hex'))
  fs.writeFileSync(secret_path, secret.toString('hex'))
  fs.chmodSync(public_path, 770)
  fs.chmodSync(secret_path, 770)
} else {
  Buffer.from(fs.readFileSync(public_path, 'utf-8'), 'hex').copy(public)
  Buffer.from(fs.readFileSync(secret_path, 'utf-8'), 'hex').copy(secret)
}

if (argv.sign) {
  const message = fs.readFileSync(Buffer.from(argv.sign))
  const signature = sodium.sodium_malloc(sodium.crypto_sign_BYTES + message.length)
  sodium.crypto_sign(signature, message, secret)
  if(argv.output) fs.writeFileSync(argv.output, signature.toString('hex'))
  else process.stdout.write(signature.toString('hex'))
} else if (argv.verify && !argv.keygen) {
  const signature = Buffer.from(fs.readFileSync(argv.verify, 'utf-8'), 'hex')
  const message = sodium.sodium_malloc(signature.length - sodium.crypto_sign_BYTES) 
  if (sodium.crypto_sign_open(message, signature, public)) {
    if (argv.output) fs.writeFileSync(argv.output)
    else process.stdout.write(message.toString('utf-8'))
  } else {
    process.exit(2)
  }
}
 
